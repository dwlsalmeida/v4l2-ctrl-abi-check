#! /bin/bash

declare -A compiler

LANG=C
CFLAGS="-g"
OUTPUT=builds

compiler[arm]="arm-linux-gnueabihf-gcc"
compiler[arm64]="aarch64-linux-gnu-gcc"
#compiler[x86]="gcc -m32"
#compiler[x64]="gcc -m64"

function error_exit {
    echo "$1" >&2   ## Send message to stderr. Exclude >&2 if you don't want it that way.
    exit
}

function run_arch {
    COMPILER=${compiler[$1]}
    SCRIPT=$2
    INPUT=$3
    FILE=$OUTPUT/$arch-$(basename -s .c $INPUT)
    STRUCTURE=$FILE-structures
    PAHOLE=$FILE-pahole

    command -v $COMPILER &> /dev/null || error_exit "$COMPILER not found"
    test -f $INPUT || error_exit "$INPUT not found"

    $COMPILER $CFLAGS $INPUT -o $FILE
    gdb -q --command $SCRIPT $FILE | tail -n +2 > $STRUCTURE
    pahole $FILE > $PAHOLE
    HASH=$(sha1sum $STRUCTURE | cut -d ' ' -f 1)
    echo -e "$arch:\t$STRUCTURE\n\tSHA1: $HASH"
    PHASH=$(sha1sum $PAHOLE | cut -d ' ' -f 1)
    echo -e "$arch:\t$PAHOLE\n\tSHA1: $PHASH"
}

mkdir -p $OUTPUT

abis=(mpeg2 h264 vp8 vp9 rkisp1)

for abi in "${abis[@]}"
do
  echo "$abi ..................."
  for arch in "${!compiler[@]}"
  do
    run_arch $arch $abi-gdb.py $abi.c
  done
done
