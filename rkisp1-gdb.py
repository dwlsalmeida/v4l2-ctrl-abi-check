#!/usr/bin/env python3

import gdb.types

def print_struct(name):
    struct = gdb.lookup_type(name)
    print(name)
    print("size %d" % struct.sizeof)
    print("\t\t\t\t\t\toffset\tsize")

    for k, v in gdb.types.deep_items(struct):
        field = struct[k]

        print("\t%-40s%d\t%d" % (field.name,
                                 field.bitpos / 8,
                                 field.type.sizeof))

    print()
print_struct("struct rkisp1_cif_isp_window")
print_struct("struct rkisp1_cif_isp_bls_fixed_val")
print_struct("struct rkisp1_cif_isp_bls_config")
print_struct("struct rkisp1_cif_isp_dpcc_methods_config")
print_struct("struct rkisp1_cif_isp_dpcc_config")
print_struct("struct rkisp1_cif_isp_gamma_corr_curve")
print_struct("struct rkisp1_cif_isp_gamma_curve_x_axis_pnts")
print_struct("struct rkisp1_cif_isp_sdg_config")
print_struct("struct rkisp1_cif_isp_lsc_config")
print_struct("struct rkisp1_cif_isp_ie_config")
print_struct("struct rkisp1_cif_isp_cproc_config")
print_struct("struct rkisp1_cif_isp_awb_meas_config")
print_struct("struct rkisp1_cif_isp_awb_gain_config")
print_struct("struct rkisp1_cif_isp_flt_config")
print_struct("struct rkisp1_cif_isp_bdm_config")
print_struct("struct rkisp1_cif_isp_ctk_config")
print_struct("struct rkisp1_cif_isp_goc_config")
print_struct("struct rkisp1_cif_isp_hst_config")
print_struct("struct rkisp1_cif_isp_aec_config")
print_struct("struct rkisp1_cif_isp_afc_config")
print_struct("struct rkisp1_cif_isp_dpf_nll")
print_struct("struct rkisp1_cif_isp_dpf_rb_flt")
print_struct("struct rkisp1_cif_isp_dpf_g_flt")
print_struct("struct rkisp1_cif_isp_dpf_gain")
print_struct("struct rkisp1_cif_isp_dpf_config")
print_struct("struct rkisp1_cif_isp_dpf_strength_config")
print_struct("struct rkisp1_cif_isp_isp_other_cfg")
print_struct("struct rkisp1_cif_isp_isp_meas_cfg")
print_struct("struct rkisp1_params_cfg")
print_struct("struct rkisp1_cif_isp_awb_meas")
print_struct("struct rkisp1_cif_isp_awb_stat")
print_struct("struct rkisp1_cif_isp_bls_meas_val")
print_struct("struct rkisp1_cif_isp_ae_stat")
print_struct("struct rkisp1_cif_isp_af_meas_val")
print_struct("struct rkisp1_cif_isp_af_stat")
print_struct("struct rkisp1_cif_isp_hist_stat")
print_struct("struct rkisp1_cif_isp_stat")
print_struct("struct rkisp1_stat_buffer")

gdb.execute("quit")
