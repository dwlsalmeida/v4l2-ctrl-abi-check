#include "rkisp1-config.h"

int main(int argc, char **argv)
{
struct rkisp1_cif_isp_window a01 = { 0 };
struct rkisp1_cif_isp_bls_fixed_val a02 = { 0 };
struct rkisp1_cif_isp_bls_config a03 = { 0 };
struct rkisp1_cif_isp_dpcc_methods_config a04 = { 0 };
struct rkisp1_cif_isp_dpcc_config a05 = { 0 };
struct rkisp1_cif_isp_gamma_corr_curve a06 = { 0 };
struct rkisp1_cif_isp_gamma_curve_x_axis_pnts a07 = { 0 };
struct rkisp1_cif_isp_sdg_config a08 = { 0 };
struct rkisp1_cif_isp_lsc_config a09 = { 0 };
struct rkisp1_cif_isp_ie_config a10 = { 0 };
struct rkisp1_cif_isp_cproc_config a11 = { 0 };
struct rkisp1_cif_isp_awb_meas_config a12 = { 0 };
struct rkisp1_cif_isp_awb_gain_config a13 = { 0 };
struct rkisp1_cif_isp_flt_config a14 = { 0 };
struct rkisp1_cif_isp_bdm_config a15 = { 0 };
struct rkisp1_cif_isp_ctk_config a16 = { 0 };
struct rkisp1_cif_isp_goc_config a17 = { 0 };
struct rkisp1_cif_isp_hst_config a18 = { 0 };
struct rkisp1_cif_isp_aec_config a19 = { 0 };
struct rkisp1_cif_isp_afc_config a20 = { 0 };
struct rkisp1_cif_isp_dpf_nll a20x = { 0 };
struct rkisp1_cif_isp_dpf_rb_flt a21 = { 0 };
struct rkisp1_cif_isp_dpf_g_flt a22 = { 0 };
struct rkisp1_cif_isp_dpf_gain a23 = { 0 };
struct rkisp1_cif_isp_dpf_config a24 = { 0 };
struct rkisp1_cif_isp_dpf_strength_config a25 = { 0 };
struct rkisp1_cif_isp_isp_other_cfg a26 = { 0 };
struct rkisp1_cif_isp_isp_meas_cfg a27 = { 0 };
struct rkisp1_params_cfg a28 = { 0 };
struct rkisp1_cif_isp_awb_meas a30 = { 0 };
struct rkisp1_cif_isp_awb_stat a31 = { 0 };
struct rkisp1_cif_isp_bls_meas_val a32 = { 0 };
struct rkisp1_cif_isp_ae_stat a33 = { 0 };
struct rkisp1_cif_isp_af_meas_val a34 = { 0 };
struct rkisp1_cif_isp_af_stat a35 = { 0 };
struct rkisp1_cif_isp_hist_stat a36 = { 0 };
struct rkisp1_cif_isp_stat a37 = { 0 };
struct rkisp1_stat_buffer a38 = { 0 };

	return 0;
}
